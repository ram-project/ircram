use cram::node::Builder;
use std::collections::HashSet;
use std::env;
use std::sync::{Arc, Mutex};

fn main() {
    let args: Vec<String> = env::args().collect();
    let port_string: String = args[1].parse().unwrap();
    let port_host: usize = port_string.parse::<usize>().unwrap();
    let node_builder = Builder::new(port_host.clone());

    let clients: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(HashSet::new()));

    let clients_clone = clients.clone();
    let node_config = node_builder.get_shallow_node();

    let connect = move |v: Vec<u8>| {
        let string = std::str::from_utf8(&v).unwrap().to_string();

        let mut clients_lock = clients_clone.lock().unwrap();
        clients_lock.insert(string);

        for ip in clients_lock.iter() {
            node_config.send(v.clone(), ip.to_string());
        }
    };

    let clients_clone1 = clients.clone();
    let node_config1 = node_builder.get_shallow_node();

    let send = move |v: Vec<u8>| {
        let response: Vec<&str> = std::str::from_utf8(&v).unwrap().trim().split(",").collect();
        let address = response[0];

        let clients_lock = clients_clone1.lock().unwrap();

        for ip in clients_lock.iter().filter(|ip| ip.to_string() != address) {
            node_config1.send_with_label(v.clone(), "read".to_string(), ip.to_string());
        }
    };

    node_builder
        .set_label_controller(|_| panic!())
        .add_label_handler("connect".to_string(), connect)
        .add_label_handler("send".to_string(), send)
        .build(2);

    loop {}
}

use cram::node::Builder;
use std::collections::HashSet;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let port_string: String = args[1].parse().unwrap();
    let port_host: usize = port_string.parse::<usize>().unwrap();
    let node_builder = Builder::new(port_host.clone());

    let mut clients: HashSet<String> = HashSet::new();

    let node_config = node_builder.get_shallow_node();

    let handler = move |v: Vec<u8>| {
        let splitted: Vec<&str> = std::str::from_utf8(&v).unwrap().trim().split(" ").collect();

        match splitted[0] {
            "connect" => {
                clients.insert(splitted[1].to_string());

                for ip in clients.iter() {
                    node_config.send(splitted[1].as_bytes().to_vec(), ip.to_string());
                }
            }

            "send" => {
                let response: Vec<&str> =
                    std::str::from_utf8(&v).unwrap().trim().split(" ").collect();
                let response: Vec<&str> = response[1].split(",").collect();
                let address = response[0];

                for ip in clients.iter().filter(|ip| ip.to_string() != address) {
                    node_config.send_with_label(v.clone(), "read".to_string(), ip.to_string());
                }
            }

            _ => println!("INVALID COMMAND"),
        }
    };

    node_builder.set_simple_controller_mut(handler).build(1);

    loop {}
}

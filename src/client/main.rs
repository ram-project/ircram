use cram::node::Builder;
use std::env;
use std::io::{self, BufRead};

fn main() {
    let args: Vec<String> = env::args().collect();
    let port_string: String = args[1].parse().unwrap();
    let port_host: usize = port_string.parse::<usize>().unwrap();

    let node_builder = Builder::new(port_host.clone());
    let node_config = node_builder.get_shallow_node();

    let server_ip = args[2].clone();
    let host = format!("localhost:{}", args[1]);
    let bytes = format!("connect {}", host);

    node_config.send(bytes.as_bytes().to_vec(), server_ip.clone());

    let read = move |v: Vec<u8>| {
        let response: Vec<&str> = std::str::from_utf8(&v).unwrap().trim().split(" ").collect();
        let response: Vec<&str> = response[1].split(",").collect();
        let address = response[0];
        let msg = response[1];
        println!("{}: {}", address, msg);
    };

    node_builder
        .set_label_controller(|_| panic!())
        .add_label_handler("read".to_string(), read)
        .build(1);

    loop {
        let stdin = io::stdin();
        for line in stdin.lock().lines() {
            let l = line.unwrap();
            let msg = format!("send {},{}", host, l).as_bytes().to_vec();
            node_config.send(msg, server_ip.clone());
        }
    }
}
